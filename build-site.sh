#!/bin/bash

cd $SOURCE

if [ -e package.json ]; then
    npm install
fi

for theme in $SOURCE/themes/*; do
    echo "Found theme: " $theme
    cd $theme
    if [ -e package.json ]; then
	npm install
	npm run-script build
    fi
    cd $SOURCE
done

hugo && rsync -avz $SOURCE/public/ $PUBLIC
