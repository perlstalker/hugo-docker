FROM registry.gitlab.com/perlstalker/hugo-docker:base

# install hugo
## install release
#ARG HUGO_VERSION=0.54.0
ARG HUGO_VERSION=0.81.0
ENV HUGO_PACKAGE=/root/hugo_${HUGO_VERSION}_Linux-64bit.deb
ENV HUGO_URL=https://github.com/spf13/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_Linux-64bit.deb
RUN wget --no-check-certificate -qO $HUGO_PACKAGE $HUGO_URL && dpkg -i $HUGO_PACKAGE && rm $HUGO_PACKAGE && hugo version

WORKDIR $SOURCE

CMD ['/usr/local/bin/build-site.sh']
